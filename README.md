# quad_scan_east_slow_ions_2023_1GeV

Measurement done with the 1.0 GeV/u ion beam to the EAST Area

[Main repo: Quad Scan East](https://gitlab.cern.ch/eljohnso/quad-scan-east)